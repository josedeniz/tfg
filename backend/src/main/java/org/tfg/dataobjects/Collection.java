package org.tfg.dataobjects;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.tfg.application.AboutDto;
import org.tfg.application.AuthorDto;
import org.tfg.application.LibsDto;
import org.tfg.application.PaperDto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity(name = "collection")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Collection {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "web")
    private String web;
    @Column(name = "description", length = 1024)
    private String description;
    @Type(type = "jsonb")
    @Column(name = "authors", columnDefinition = "jsonb")
    private List<AuthorDto> authors;
    @Type(type = "jsonb")
    @Column(name = "papers", columnDefinition = "jsonb")
    private List<PaperDto> papers;
    @Type(type = "jsonb")
    @Column(name = "libs", columnDefinition = "jsonb")
    private List<LibsDto> libs;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "collection_dataset",
            joinColumns = { @JoinColumn(name = "collection_id") },
            inverseJoinColumns = { @JoinColumn(name = "dataset_id") })
    private Set<Dataset> datasets = new HashSet<>();

    // Necessary for JPA
    public Collection() {
        this.name = "";
        this.web = "";
        this.description = "";
        this.authors = new ArrayList<>();
        this.papers = new ArrayList<>();
        this.libs = new ArrayList<>();
    }

    public Collection(String name, String web, String description, List<AuthorDto> authors, List<PaperDto> papers, List<LibsDto> libs) {
        this.name = name;
        this.web = web;
        this.description = description;
        this.authors = authors;
        this.papers = papers;
        this.libs = libs;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Collection setName(String name) {
        this.name = name;
        return this;
    }

    public Set<Dataset> getDatasets() {
        return datasets;
    }

    public Collection setDatasets(Set<Dataset> datasets) {
        this.datasets = datasets;
        return this;
    }

    public AboutDto getAbout() {
        return new AboutDto(this.web, this.description, this.authors, this.papers, this.libs);
    }
}
