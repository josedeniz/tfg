package org.tfg.dataobjects;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Entity(name = "researcher")
public class Researcher {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "email", unique = true)
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "enabled")
    private Boolean enabled;
    @Column(name = "roles")
    private String roles;

    @Transient
    private final String ROLES_DELIMITER = " ";

    public Researcher() {
        this.name = "";
        this.email = "";
        this.password = "";
        this.enabled = true;
        this.roles = Role.USER.getValue();
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Researcher setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return this.email;
    }

    public Researcher setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return this.password;
    }

    public Researcher setPassword(String password) {
        this.password = password;
        return this;
    }

    public List<Role> getRoles() {
        return Arrays.stream(this.roles.split(ROLES_DELIMITER))
                .map(Role::fromValue).collect(toList());
    }

    public Researcher setRoles(Role... roles) {
        this.roles = Arrays.stream(roles)
                .map(Role::getValue)
                .collect(Collectors.joining(ROLES_DELIMITER));
        return this;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public Researcher enabled() {
        this.enabled = true;
        return this;
    }

    public Researcher disabled() {
        this.enabled = false;
        return this;
    }
}
