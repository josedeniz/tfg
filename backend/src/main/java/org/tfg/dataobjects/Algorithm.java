package org.tfg.dataobjects;

import javax.persistence.*;

@Entity(name = "algorithm")
public class Algorithm {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "filename")
    private String filename;
    @Column(name = "original_filename")
    private String originalFilename;
    @Column(name = "researcher_ids")
    private String researcherIds;
    @Column(name = "dataset_ids")
    private String datasetIds;

    public Algorithm() {
        filename = "";
        originalFilename = "";
        researcherIds = "";
        datasetIds = "";
    }

    public Long getId() {
        return id;
    }

    public String getFilename() {
        return filename;
    }

    public Algorithm setFilename(String filename) {
        this.filename = filename;
        return this;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public Algorithm setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
        return this;
    }

    public String getResearcherIds() {
        return researcherIds;
    }

    public Algorithm setResearcherIds(String researcherIds) {
        this.researcherIds = researcherIds;
        return this;
    }

    public String getDatasetIds() {
        return datasetIds;
    }

    public Algorithm setDatasetIds(String datasetIds) {
        this.datasetIds = datasetIds;
        return this;
    }

    public String getName() {
        return originalFilename.split("\\.")[0];
    }
}
