package org.tfg.dataobjects;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "dataset")
public class Dataset {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "category_ids")
    private String categoryIds;
    @Column(name = "has_video")
    private boolean hasVideo;
    @Column(name = "video_id")
    private String videoId;
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "datasets")
    private Set<Collection> collections = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "dataset_media",
            joinColumns = { @JoinColumn(name = "dataset_id") },
            inverseJoinColumns = { @JoinColumn(name = "media_id") })
    private Set<Media> media = new HashSet<>();

    public Dataset() {
        name = "";
        categoryIds = "";
        videoId = "";
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Dataset setName(String name) {
        this.name = name;
        return this;
    }

    public String getCategoryIds() {
        return categoryIds;
    }

    public Dataset setCategoryIds(String categoryIds) {
        this.categoryIds = categoryIds;
        return this;
    }

    public Set<Collection> getCollections() {
        return collections;
    }

    public Dataset setCollections(Set<Collection> collections) {
        this.collections = collections;
        return this;
    }

    public Set<Media> getMedia() {
        return media;
    }

    public Dataset setMedia(Set<Media> media) {
        this.media = media;
        return this;
    }

    public void addMedia(Media media) {
        this.media.add(media);
    }

    public boolean isInCollection(Long collectionId) {
        return collections.stream().anyMatch(c -> c.getId().equals(collectionId));
    }

    public boolean hasVideo() {
        return hasVideo;
    }

    public Dataset setHasVideo(boolean hasVideo) {
        this.hasVideo = hasVideo;
        return this;
    }

    public String getVideoId() {
        return videoId;
    }

    public Dataset setVideoId(String videoId) {
        this.videoId = videoId;
        return this;
    }
}
