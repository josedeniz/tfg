package org.tfg.dataobjects;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "media")
public class Media {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "extension")
    private String extension;
    @Column(name = "originalFilename")
    private String originalFilename;
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "media")
    private Set<Dataset> datasets = new HashSet<>();


    public static Media create(String fileName, String originalFilename) {
        final String[] nameAndExtension = fileName.split("\\.");
        return new Media()
                .setOriginalFilename(originalFilename)
                .setName(nameAndExtension[0])
                .setExtension(nameAndExtension[1]);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Media setName(String name) {
        this.name = name;
        return this;
    }

    public String getExtension() {
        return extension;
    }

    public Media setExtension(String extension) {
        this.extension = extension;
        return this;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public Media setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
        return this;
    }

    public Set<Dataset> getDatasets() {
        return datasets;
    }

    public Media setDatasets(Set<Dataset> datasets) {
        this.datasets = datasets;
        return this;
    }

    public String getResourceFilename() {
        return String.format("%s.%s", getName(), getExtension());
    }

    public boolean isForDataset(Long datasetId) {
        return datasets.stream().anyMatch(d -> d.getId().equals(datasetId));
    }
}
