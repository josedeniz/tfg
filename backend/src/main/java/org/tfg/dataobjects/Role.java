package org.tfg.dataobjects;

public enum Role {
    ADMIN("ROLE_ADMIN"), DATABASE_MANAGER("ROLE_DB_MANAGER"), USER("ROLE_USER");

    private String value;

    Role(String value) {
        this.value = value;
    }

    public static Role fromValue(String value) {
        if (value.equals(ADMIN.getValue())) return ADMIN;
        if (value.equals(DATABASE_MANAGER.getValue())) return DATABASE_MANAGER;
        return USER;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
