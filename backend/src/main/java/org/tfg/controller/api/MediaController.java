package org.tfg.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.tfg.application.ResourceDto;
import org.tfg.application.ResourceIdDto;
import org.tfg.services.GetMedia;
import org.tfg.services.UploadMedia;

import java.util.List;

@RestController
@RequestMapping("${spring.data.rest.base-path}/media")
public class MediaController {

    private final UploadMedia uploadMedia;
    private final GetMedia getMedia;

    @Autowired
    public MediaController(UploadMedia uploadMedia, GetMedia getMedia) {
        this.uploadMedia = uploadMedia;
        this.getMedia = getMedia;
    }

    @PostMapping("/upload")
    @PreAuthorize("hasRole('DB_MANAGER')")
    public void uploadMedia(@RequestParam(value = "datasetId", required = false) String datasetId, @RequestPart(value = "file") MultipartFile file) {
        uploadMedia.Execute(datasetId, file);
    }

    @GetMapping()
    public List<ResourceIdDto> getResources() {
        return getMedia.getMediaResources();
    }

    @GetMapping("/datasets/{datasetId}")
    public List<ResourceIdDto> getResourcesByDataset(@PathVariable("datasetId") Long datasetId) {
        return getMedia.getMediaResourcesByDataset(datasetId);
    }

    @GetMapping("/{resourceId}")
    public ResourceDto getResource(@PathVariable("resourceId") String resourceId) {
        return getMedia.getMediaResource(resourceId);
    }
}
