package org.tfg.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.tfg.application.AlgorithmDto;
import org.tfg.dataobjects.Researcher;
import org.tfg.services.GetAlgorithms;
import org.tfg.services.TokenManager;
import org.tfg.services.UploadAlgorithm;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("${spring.data.rest.base-path}/algorithms")
public class AlgorithmController {

    private final UploadAlgorithm uploadAlgorithm;
    private final TokenManager tokenManager;
    private final GetAlgorithms getAlgorithms;

    @Autowired
    public AlgorithmController(UploadAlgorithm uploadAlgorithm, TokenManager tokenManager, GetAlgorithms getAlgorithms) {
        this.uploadAlgorithm = uploadAlgorithm;
        this.tokenManager = tokenManager;
        this.getAlgorithms = getAlgorithms;
    }

    @PostMapping("/upload")
    @PreAuthorize("hasRole('DB_MANAGER')")
    public void uploadFile(@RequestParam(value = "datasetId") String datasetId, @RequestPart(value = "file") MultipartFile file, HttpServletRequest request) {
        final Researcher userFromToken = tokenManager.getUserFromToken(request);
        uploadAlgorithm.Execute(userFromToken.getId(), datasetId, file);
    }

    @GetMapping
    public List<AlgorithmDto> getAllAlgorithmsForUserId(HttpServletRequest request) {
        final Researcher userFromToken = tokenManager.getUserFromToken(request);
        return getAlgorithms.execute(userFromToken.getId());
    }

}
