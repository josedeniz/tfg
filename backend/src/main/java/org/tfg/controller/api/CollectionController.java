package org.tfg.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tfg.application.CollectionDetailDto;
import org.tfg.application.CollectionDto;
import org.tfg.application.DatasetDetailDto;
import org.tfg.application.DatasetDto;
import org.tfg.services.CollectionsService;
import org.tfg.services.DatasetsService;

import java.util.List;

@RestController
@RequestMapping("${spring.data.rest.base-path}/collections")
public class CollectionController {

    private final CollectionsService collectionsService;
    private final DatasetsService datasetsService;

    @Autowired
    public CollectionController(CollectionsService collectionsService, DatasetsService datasetsService) {
        this.collectionsService = collectionsService;
        this.datasetsService = datasetsService;
    }

    @GetMapping()
    public List<CollectionDto> getCollections() {
        return collectionsService.getCollections();
    }

    @GetMapping("/{id}")
    public CollectionDetailDto getCollection(@PathVariable("id") Long id) {
        return collectionsService.getCollection(id);
    }

    @GetMapping("/{collectionId}/datasets")
    public List<DatasetDto> getDatasets(@PathVariable("collectionId") Long collectionId) {
        return datasetsService.getDatasets(collectionId);
    }

    @GetMapping("/{collectionId}/datasets/{datasetId}")
    public DatasetDetailDto getDataset(@PathVariable("datasetId") Long datasetId) {
        return datasetsService.getDataset(datasetId);
    }

}
