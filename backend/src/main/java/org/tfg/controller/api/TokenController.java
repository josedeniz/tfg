package org.tfg.controller.api;

import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.tfg.infrastructure.security.JwtAuthenticationResponse;
import org.tfg.infrastructure.security.JwtRefreshTokenRequest;
import org.tfg.infrastructure.security.JwtTokenUtil;

@RestController
public class TokenController implements ApiController {

    private final JwtTokenUtil jwtTokenUtil;

    @Autowired
    public TokenController(JwtTokenUtil jwtTokenUtil) {
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @PostMapping("/token/refresh")
    public ResponseEntity<?> refreshToken(@RequestBody JwtRefreshTokenRequest refreshTokenRequest) {
        final String token;
        try {
            token = jwtTokenUtil.refreshExpiredToken(refreshTokenRequest.getToken());
        } catch (ExpiredJwtException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
        }
        final String refreshToken = jwtTokenUtil.generateRefreshFromAuthorizationToken(token);
        return ResponseEntity.ok(new JwtAuthenticationResponse(token, refreshToken));
    }
}
