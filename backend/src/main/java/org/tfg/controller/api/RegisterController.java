package org.tfg.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.tfg.dataobjects.Researcher;
import org.tfg.infrastructure.persistence.ResearcherRepository;

@RestController
public class RegisterController implements ApiController {

    private final PasswordEncoder passwordEncoder;
    private final ResearcherRepository researcherRepository;

    @Autowired
    public RegisterController(PasswordEncoder passwordEncoder, ResearcherRepository researcherRepository) {
        this.passwordEncoder = passwordEncoder;
        this.researcherRepository = researcherRepository;
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody Researcher user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Researcher persistedUser;
        try {
            persistedUser = researcherRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
        return ResponseEntity.ok(new RegisterResponse().setEmail(persistedUser.getEmail()));
    }

    class RegisterResponse {
        private String email;

        private RegisterResponse() {
            this.email = "";
        }

        public String getEmail() {
            return email;
        }

        public RegisterResponse setEmail(String email) {
            this.email = email;
            return this;
        }
    }
}
