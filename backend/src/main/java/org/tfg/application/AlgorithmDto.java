package org.tfg.application;

import java.util.List;

public class AlgorithmDto {
    public String filename;
    public List<String> datasets;

    public AlgorithmDto(String filename, List<String> datasets) {
        this.filename = filename;
        this.datasets = datasets;
    }
}
