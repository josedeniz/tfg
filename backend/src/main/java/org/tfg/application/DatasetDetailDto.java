package org.tfg.application;

import java.util.List;

public class DatasetDetailDto {
    public String id;
    public String name;
    public List<String> thumbnails;
    public boolean hasVideo;
    public String videoId;
    public RankingDto ranking;

    public DatasetDetailDto(String id, String name, List<String> thumbnails, boolean hasVideo, String videoId, RankingDto ranking) {
        this.id = id;
        this.name = name;
        this.thumbnails = thumbnails;
        this.hasVideo = hasVideo;
        this.videoId = videoId;
        this.ranking = ranking;
    }
}
