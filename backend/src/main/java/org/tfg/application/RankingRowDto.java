package org.tfg.application;

import java.util.List;

public class RankingRowDto {
    public String name;
    public List<Double> evaluations;

    public RankingRowDto(String name, List<Double> evaluations) {
        this.name = name;
        this.evaluations = evaluations;
    }
}
