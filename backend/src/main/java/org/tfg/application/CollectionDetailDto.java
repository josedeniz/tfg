package org.tfg.application;

public class CollectionDetailDto {
    public String id;
    public String name;
    public AboutDto about;

    public CollectionDetailDto(String id, String name, AboutDto about) {
        this.id = id;
        this.name = name;
        this.about = about;
    }
}
