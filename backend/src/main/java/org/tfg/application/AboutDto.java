package org.tfg.application;

import java.util.List;

public class AboutDto {
    public String web;
    public String description;
    public List<AuthorDto> authors;
    public List<PaperDto> papers;
    public List<LibsDto> libs;

    public AboutDto(String web, String description, List<AuthorDto> authors, List<PaperDto> papers, List<LibsDto> libs) {
        this.web = web;
        this.description = description;
        this.authors = authors;
        this.papers = papers;
        this.libs = libs;
    }
}
