package org.tfg.application;

import java.io.Serializable;

public class AuthorDto implements Serializable {
    public String name;
    public String bio;

    public AuthorDto() {
        this.name = "";
        this.bio = "";
    }

    public AuthorDto(String name) {
        this.name = name;
        this.bio = "";
    }

    public AuthorDto(String name, String bio) {
        this.name = name;
        this.bio = bio;
    }
}
