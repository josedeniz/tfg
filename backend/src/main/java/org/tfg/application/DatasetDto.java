package org.tfg.application;

public class DatasetDto {
    public String id;
    public String name;
    public String thumbnail;

    public DatasetDto(String id, String name, String thumbnail) {
        this.id = id;
        this.name = name;
        this.thumbnail = thumbnail;
    }
}
