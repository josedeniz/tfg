package org.tfg.application;

public class ResourceIdDto {
    public String resourceId;
    public String filename;

    public ResourceIdDto(String resourceId, String filename) {
        this.resourceId = resourceId;
        this.filename = filename;
    }
}
