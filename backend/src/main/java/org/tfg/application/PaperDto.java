package org.tfg.application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PaperDto implements Serializable {
    public String name;
    public List<String> lines;

    public PaperDto() {
        this.name = "";
        this.lines = new ArrayList<>();
    }

    public PaperDto(String name, List<String> lines) {
        this.name = name;
        this.lines = lines;
    }
}
