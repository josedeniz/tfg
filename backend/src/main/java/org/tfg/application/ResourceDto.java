package org.tfg.application;

public class ResourceDto {
    public String filename;
    public String base64Resource;

    public ResourceDto() {
        filename = "";
        base64Resource = "";
    }

    public ResourceDto(String filename, String base64Resource) {
        this.filename = filename;
        this.base64Resource = base64Resource;
    }
}
