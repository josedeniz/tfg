package org.tfg.application;

import java.util.List;

public class RankingDto {
    public List<String> headerColumnNames;
    public List<RankingRowDto> rankingRows;

    public RankingDto(List<String> headerColumnNames, List<RankingRowDto> rankingRowDtos) {
        this.headerColumnNames = headerColumnNames;
        this.rankingRows = rankingRowDtos;
    }
}
