package org.tfg.application;

public class CollectionDto {
    public String id;
    public String name;

    public CollectionDto(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
