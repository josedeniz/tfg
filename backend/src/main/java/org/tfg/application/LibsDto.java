package org.tfg.application;

import java.io.Serializable;

public class LibsDto implements Serializable {
    public String name;
    public String description;

    public LibsDto() {
        this.name = "";
        this.description = "";
    }

    public LibsDto(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
