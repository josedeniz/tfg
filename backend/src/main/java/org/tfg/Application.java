package org.tfg;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.tfg.application.AuthorDto;
import org.tfg.application.LibsDto;
import org.tfg.application.PaperDto;
import org.tfg.dataobjects.*;
import org.tfg.infrastructure.persistence.*;

import java.util.ArrayList;
import java.util.HashSet;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.tfg.dataobjects.Role.*;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner insertInitialData(CollectionRepository collectionRepository, DatasetRepository datasetRepository, ResearcherRepository researcherRepository, PasswordEncoder passwordEncoder, CategoryRepository categoryRepository, MediaRepository mediaRepository){
        return args -> {
            final String emailDomain = "gmail.com";
            final String adminEmail = String.format("jose@%s", emailDomain);
            boolean hasInsertedInitialData = researcherRepository.findByEmail(adminEmail) != null;
            if (hasInsertedInitialData) return;

            // Insert Researcher and admin
            final Researcher guest = new Researcher()
                    .setName("Maria")
                    .setEmail(String.format("maria@%s", emailDomain))
                    .setPassword(passwordEncoder.encode("123"));
            final Researcher admin = new Researcher()
                    .setName("Jose")
                    .setEmail(adminEmail)
                    .setRoles(ADMIN, DATABASE_MANAGER, USER)
                    .setPassword(passwordEncoder.encode("123"));
            researcherRepository.save(asList(guest, admin));

            // Insert categories
            final Category opticalFlowCategory = new Category("Category.OpticalFlow");
            categoryRepository.save(opticalFlowCategory);

            // Insert collections
            final Collection middlebury = new Collection(
                    "Middlebury",
                    "http://vision.middlebury.edu/flow/",
                    "Nuestro trabajo fue presentado por primera vez en el ICCV 2007, donde evaluamos un pequeño conjunto de algoritmos sobre un conjunto de datos preliminar. Desde entonces, hemos creado una nueva colección de conjuntos de datos de flujo óptico con la verdad sobre el terreno. Se dividen en un set de evaluación (test), utilizado para la evaluación oficial, y un set de \"otro\" (formación). Sólo damos la verdad básica para esto último.",
                    asList(
                            createAuthor("Simon Baker"),
                            createAuthor("Daniel Scharstein", "Charles A. Dana Professor of Computer Science and Department Chair"),
                            createAuthor("JP Lewis"),
                            createAuthor("Stefan Roth"),
                            createAuthor("Michael Black"),
                            createAuthor("Richard Szeliski")
                    ),
                    new ArrayList<>(),
                    new ArrayList<>()
            );
            final Collection sintel = new Collection(
                    "Sintel",
                    "http://sintel.is.tue.mpg.de/",
                    "El conjunto de datos de MPI Sintel aborda las limitaciones de los puntos de referencia de flujo óptico existentes. Proporciona secuencias de vídeo naturalistas que son un reto para los métodos actuales. Está diseñado para fomentar la investigación sobre el movimiento de largo alcance, el desenfoque de movimiento, el análisis de múltiples fotogramas y el movimiento no rígido.\n" +
                    "El conjunto de datos contiene campos de flujo, límites de movimiento, regiones sin igual y secuencias de imágenes. Las secuencias de imágenes se representan con diferentes niveles de dificultad.\n" +
                    "Sintel es un cortometraje de animación de código abierto producido por Ton Roosendaal y la Blender Foundation. Aquí hemos modificado la película de muchas maneras para que sea útil para la evaluación del flujo óptico.",
                    asList(
                            createAuthor("Dan Butler", "Ph.D. Student, University of Washington"),
                            createAuthor("Jonas Wulff", "Ph.D. Student, Max Planck Institute for Intelligent Systems"),
                            createAuthor("Garrett Stanley", "Professor, Georgia Institute of Technology"),
                            createAuthor("Michael J. Black", "Web Developer, Max Planck Institute for Intelligent Systems"),
                            createAuthor("Oana Broscaru", "Ph.D. Student, University of Washington"),
                            createAuthor("Jon Anning", "Ops Engineer, Max Planck Institute for Intelligent SystemsOps Engineer, Max Planck Institute for Intelligent Systems")
                    ),
                    asList(
                            createPaper(
                                    "Main paper and benchmark",
                                    "@inproceedings{Butler:ECCV:2012,",
                                    "  title = {A naturalistic open source movie for optical flow evaluation},",
                                    "  author = {Butler, D. J. and Wulff, J. and Stanley, G. B. and Black, M. J.},",
                                    "  booktitle = {European Conf. on Computer Vision (ECCV)},",
                                    "  editor = {{A. Fitzgibbon et al. (Eds.)}},",
                                    "  publisher = {Springer-Verlag},",
                                    "  series = {Part IV, LNCS 7577},",
                                    "  month = oct,",
                                    "  pages = {611--625},",
                                    "  year = {2012}",
                                    "}"
                            ),
                            createPaper(
                                    "Technical background",
                                    "@inproceedings{Wulff:ECCVws:2012,",
                                    "  title = {Lessons and insights from creating a synthetic optical flow benchmark},",
                                    "  author = {Wulff, J. and Butler, D. J. and Stanley, G. B. and Black, M. J.},",
                                    "  booktitle = {ECCV Workshop on Unsolved Problems in Optical Flow and Stereo Estimation},",
                                    "  editor = {{A. Fusiello et al. (Eds.)}},",
                                    "  publisher = {Springer-Verlag},",
                                    "  series = {Part II, LNCS 7584},",
                                    "  month = oct,",
                                    "  pages = {168--177},",
                                    "  year = {2012}",
                                    "}"
                            )
                    ),
                    singletonList(
                            new LibsDto(
                                    "MPI-Sintel-bundler (2.6MB)",
                                    "La carga de los campos de flujo calculados para el equipo de prueba completo no es práctica debido a la cantidad de datos. En consecuencia, proporcionamos un empaquetador que submuestrea los datos del campo de flujo óptico. Este muestreo de los datos está diseñado para imitar las estadísticas del conjunto completo de datos.\n" +
                                            "El empaquetador también genera un único archivo para su presentación en el formato apropiado. El archivo incluye ejecutables binarios para Linux, OSX y Windows.\n" +
                                            "Nota: El empaquetador también se encuentra en el archivo de datos completos y de prueba (véase más arriba); no es necesario descargarlo dos veces."
                            )
                    )
            );
            collectionRepository.save(asList(middlebury, sintel));

            // Insert Datasets
            final Dataset armyDataset = new Dataset()
                    .setName("Army")
                    .setCategoryIds(opticalFlowCategory.getId().toString());
            final Dataset teddyDataset = new Dataset()
                    .setName("Teddy")
                    .setCategoryIds(opticalFlowCategory.getId().toString());
            final Dataset adventurerDataset = new Dataset()
                    .setName("Adventurer")
                    .setHasVideo(true)
                    .setVideoId("ZmiBI4tPk_o")
                    .setCategoryIds(opticalFlowCategory.getId().toString());
            datasetRepository.save(asList(armyDataset, teddyDataset, adventurerDataset));

            // Insert relation Collection <-> Dataset
            middlebury.setDatasets(new HashSet<>(asList(armyDataset, teddyDataset)));
            sintel.setDatasets(new HashSet<>(singletonList(adventurerDataset)));
            collectionRepository.save(asList(middlebury, sintel));

            // Insert media
            final Media army = Media.create("army.jpg", "army.jpg");
            final Media teddy = Media.create("teddy.png", "teddy.png");
            final Media adventurer = Media.create("adventurer.png", "adventurer.png");
            mediaRepository.save(asList(army, teddy, adventurer));

            // Insert relation Dataset <-> Media
            armyDataset.addMedia(army);
            teddyDataset.addMedia(teddy);
            adventurerDataset.addMedia(adventurer);
            datasetRepository.save(asList(armyDataset, teddyDataset, adventurerDataset));
        };
    }

    private AuthorDto createAuthor(String name) {
        return new AuthorDto(name);
    }

    private AuthorDto createAuthor(String name, String bio) {
        return new AuthorDto(name, bio);
    }

    private PaperDto createPaper(String name, String... lines) {
        return new PaperDto(name, asList(lines));
    }

}
