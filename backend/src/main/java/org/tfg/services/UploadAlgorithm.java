package org.tfg.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.tfg.services.mediastorage.StorageManager;
import org.tfg.services.mediastorage.UniqueFilenameGenerator;

@Service
public class UploadAlgorithm {
    private final UniqueFilenameGenerator uniqueFilenameGenerator;
    private final StorageManager storageManager;
    private final SaveAlgorithm saveAlgorithm;

    @Autowired
    public UploadAlgorithm(UniqueFilenameGenerator uniqueFilenameGenerator, StorageManager storageManager, SaveAlgorithm saveAlgorithm) {
        this.uniqueFilenameGenerator = uniqueFilenameGenerator;
        this.storageManager = storageManager;
        this.saveAlgorithm = saveAlgorithm;
    }

    public void Execute(Long researcherId, String datasetId, MultipartFile file) {
        final String originalFilename = file.getOriginalFilename();
        final String filename = uniqueFilenameGenerator.generateUniqueFileName(originalFilename);
        storageManager.uploadFile(filename, file);
        saveAlgorithm.save(filename, originalFilename, datasetId, researcherId);
    }
}
