package org.tfg.services.mediastorage;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.springframework.web.multipart.MultipartFile;
import org.tfg.application.ResourceDto;
import org.tfg.application.ResourceIdDto;
import org.tfg.dataobjects.Media;
import org.tfg.infrastructure.persistence.MediaRepository;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class S3StorageManager implements StorageManager {

    private final String bucketName;
    private final AmazonS3 s3client;

    public S3StorageManager(String accessKey, String secretKey, String bucketName) {
        this.bucketName = bucketName;
        BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        this.s3client = AmazonS3ClientBuilder
                .standard()
                .withRegion(Regions.EU_WEST_1)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();
    }

    @Override
    public List<ResourceIdDto> getMediaResources(Iterable<Media> all) {
        List<ResourceIdDto> resourceIds = new ArrayList<>();
        all.forEach(media -> resourceIds.add(createResourceId(media)));
        return resourceIds;
    }

    @Override
    public List<ResourceIdDto> getMediaResourcesByDataset(Long datasetId, Iterable<Media> all) {
        List<ResourceIdDto> resourceIds = new ArrayList<>();
        all.forEach(media -> {
            if (media.isForDataset(datasetId)) resourceIds.add(createResourceId(media));
        });
        return resourceIds;
    }

    private ResourceIdDto createResourceId(Media media) {
        return new ResourceIdDto(media.getName(), media.getOriginalFilename());
    }

    @Override
    public void uploadFile(String fileName, MultipartFile multipartFile) {
        try {
            uploadFileTos3bucket(fileName, multipartFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ResourceDto getMediaResource(Media media) {
        final InputStream inputStream = s3client.getObject(new GetObjectRequest(bucketName, media.getResourceFilename())).getObjectContent();
        final String base64Resource = InputStreamToBase64Converter.convert(inputStream);
        return new ResourceDto(media.getOriginalFilename(), base64Resource);
    }

    private void uploadFileTos3bucket(String fileName, MultipartFile multipartFile) throws IOException {
        s3client.putObject(new PutObjectRequest(bucketName, fileName, convertMultiPartToFile(multipartFile))
                .withCannedAcl(CannedAccessControlList.PublicRead));
    }

    private File convertMultiPartToFile(MultipartFile multipart) throws IOException {
        File file = File.createTempFile(multipart.getOriginalFilename(), ".tmp");
        file.deleteOnExit();
        multipart.transferTo(file);
        return file;
    }
}
