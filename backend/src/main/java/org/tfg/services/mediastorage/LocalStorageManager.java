package org.tfg.services.mediastorage;

import org.springframework.web.multipart.MultipartFile;
import org.tfg.application.ResourceDto;
import org.tfg.application.ResourceIdDto;
import org.tfg.dataobjects.Media;
import org.tfg.infrastructure.persistence.MediaRepository;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class LocalStorageManager implements StorageManager {

    private final String storageLocalDirectory;

    public LocalStorageManager(String storageLocalDirectory) {
        this.storageLocalDirectory = storageLocalDirectory;
    }

    @Override
    public ResourceDto getMediaResource(Media media) {
        final Path filename = Paths.get(storageLocalDirectory, media.getResourceFilename());
        try {
            final InputStream inputStream = new FileInputStream(filename.toFile());
            final String base64Resource = InputStreamToBase64Converter.convert(inputStream);
            return new ResourceDto(media.getOriginalFilename(), base64Resource);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return new ResourceDto();
    }

    @Override
    public List<ResourceIdDto> getMediaResources(Iterable<Media> all) {
        List<ResourceIdDto> resourceIds = new ArrayList<>();
        all.forEach(media -> resourceIds.add(new ResourceIdDto(media.getName(), media.getOriginalFilename())));
        return resourceIds;
    }

    @Override
    public List<ResourceIdDto> getMediaResourcesByDataset(Long datasetId, Iterable<Media> all) {
        List<ResourceIdDto> resourceIds = new ArrayList<>();
        all.forEach(media -> {
            if (media.isForDataset(datasetId))
                resourceIds.add(new ResourceIdDto(media.getName(), media.getOriginalFilename()));
        });
        return resourceIds;
    }

    @Override
    public void uploadFile(String fileName, MultipartFile multipartFile) {
        try {
            final Path filePath = Paths.get(storageLocalDirectory, fileName);
            Files.write(filePath, multipartFile.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
