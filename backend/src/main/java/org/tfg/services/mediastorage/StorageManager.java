package org.tfg.services.mediastorage;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.tfg.application.ResourceDto;
import org.tfg.application.ResourceIdDto;
import org.tfg.dataobjects.Media;

import java.util.List;

@Service
public interface StorageManager {
    void uploadFile(String fileName, MultipartFile multipartFile);
    ResourceDto getMediaResource(Media media);
    List<ResourceIdDto> getMediaResources(Iterable<Media> all);
    List<ResourceIdDto> getMediaResourcesByDataset(Long datasetId, Iterable<Media> all);
}
