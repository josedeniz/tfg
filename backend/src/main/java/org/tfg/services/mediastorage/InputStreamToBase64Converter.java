package org.tfg.services.mediastorage;

import com.amazonaws.util.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

public class InputStreamToBase64Converter {
    public static String convert(InputStream inputStream) {
        try {
            final byte[] bytes = IOUtils.toByteArray(inputStream);
            return Base64.getEncoder().encodeToString(bytes);
        } catch (IOException e) {
            return "AnErrorOccurredConvertingToBase64";
        }
    }
}
