package org.tfg.services.mediastorage;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.UUID;

@Component
public class UniqueFilenameGenerator {

    public String generateUniqueFileName(String originalFilename) {
        final String filename = UUID.randomUUID().toString();
        final String extension = originalFilename.substring(originalFilename.lastIndexOf(".")).toLowerCase();
        return filename + extension;
    }
}
