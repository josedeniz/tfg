package org.tfg.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tfg.infrastructure.config.ConfigurationException;
import org.tfg.services.mediastorage.LocalStorageManager;
import org.tfg.services.mediastorage.S3StorageManager;
import org.tfg.services.mediastorage.StorageManager;

import java.io.File;

import static java.lang.String.format;

@Configuration
public class MediaServiceFactory {

    @Value("${storage.mode}")
    private String storageMode;
    @Value("${storage.local.directory}")
    private String storageLocalDirectory;
    @Value("${storage.aws.s3.bucket.name}")
    private String bucketName;
    @Value("${storage.aws.access.key.id}")
    private String accessKey;
    @Value("${storage.aws.access.key.secret}")
    private String secretKey;

    private final String AWS = "aws";
    private final String LOCAL = "local";

    @Bean
    public StorageManager storageManager() throws ConfigurationException {
        final String mode = storageMode.toLowerCase().trim();
        if (mode.equals(LOCAL)) return createLocalStorageManager();
        if (mode.equals(AWS)) return new S3StorageManager(accessKey, secretKey, bucketName);
        throw new ConfigurationException(format("Error configuring StorageManager. Allowed values [%s, %s], please review if you have one of them. Current value: [%s]", AWS, LOCAL, mode));
    }

    private StorageManager createLocalStorageManager() throws ConfigurationException {
        final String storageLocalDirectoryConfigKey = "storage.local.directory";
        final File folder = new File(storageLocalDirectory);
        if (!folder.exists()) throw new ConfigurationException(format("%s [%s] not exists", storageLocalDirectoryConfigKey, storageLocalDirectory));
        if (!folder.isDirectory()) throw new ConfigurationException(format("%s [%s] is not a directory", storageLocalDirectoryConfigKey, storageLocalDirectory));
        return new LocalStorageManager(storageLocalDirectory);
    }

}
