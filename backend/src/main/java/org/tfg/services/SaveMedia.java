package org.tfg.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tfg.dataobjects.Dataset;
import org.tfg.dataobjects.Media;
import org.tfg.infrastructure.persistence.DatasetRepository;
import org.tfg.infrastructure.persistence.MediaRepository;

@Service
public class SaveMedia {

    private final MediaRepository mediaRepository;
    private final DatasetRepository datasetRepository;

    @Autowired
    public SaveMedia(MediaRepository mediaRepository, DatasetRepository datasetRepository) {
        this.mediaRepository = mediaRepository;
        this.datasetRepository = datasetRepository;
    }

    public void save(String fileName, String originalFilename) {
        mediaRepository.save(Media.create(fileName, originalFilename));
    }

    public void save(String fileName, String originalFilename, Long datasetId) {
        final Media media = Media.create(fileName, originalFilename);
        mediaRepository.save(media);
        final Dataset dataset = datasetRepository.findOne(datasetId);
        if (dataset == null) return;
        dataset.addMedia(media);
        datasetRepository.save(dataset);
    }
}
