package org.tfg.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.tfg.services.mediastorage.StorageManager;
import org.tfg.services.mediastorage.UniqueFilenameGenerator;

@Service
public class UploadMedia {
    private final UniqueFilenameGenerator uniqueFilenameGenerator;
    private final StorageManager storageManager;
    private final SaveMedia saveMedia;

    @Autowired
    public UploadMedia(UniqueFilenameGenerator uniqueFilenameGenerator, StorageManager storageManager, SaveMedia saveMedia) {
        this.uniqueFilenameGenerator = uniqueFilenameGenerator;
        this.storageManager = storageManager;
        this.saveMedia = saveMedia;
    }

    public void Execute(String datasetId, MultipartFile file) {
        final String originalFilename = file.getOriginalFilename();
        final String filename = uniqueFilenameGenerator.generateUniqueFileName(originalFilename);
        storageManager.uploadFile(filename, file);
        if (datasetId != null && !datasetId.equals("")) {
            saveMedia.save(filename, originalFilename, Long.parseLong(datasetId));
        } else saveMedia.save(filename, originalFilename);
    }
}
