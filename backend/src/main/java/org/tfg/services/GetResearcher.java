package org.tfg.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tfg.dataobjects.Researcher;
import org.tfg.infrastructure.persistence.ResearcherRepository;

@Service
public class GetResearcher {

    private final ResearcherRepository researcherRepository;

    @Autowired
    public GetResearcher(ResearcherRepository researcherRepository) {
        this.researcherRepository = researcherRepository;
    }

    public Researcher execute(String email) {
        return researcherRepository.findByEmail(email);
    }
}
