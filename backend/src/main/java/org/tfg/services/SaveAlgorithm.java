package org.tfg.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tfg.dataobjects.Algorithm;
import org.tfg.infrastructure.persistence.AlgorithmRepository;

@Service
public class SaveAlgorithm {

    private final AlgorithmRepository algorithmRepository;

    @Autowired
    public SaveAlgorithm(AlgorithmRepository algorithmRepository) {
        this.algorithmRepository = algorithmRepository;
    }

    public void save(String fileName, String originalFilename, String datasetId, Long researcherId) {
        final Algorithm algorithm = new Algorithm()
                .setFilename(fileName)
                .setOriginalFilename(originalFilename)
                .setDatasetIds(datasetId)
                .setResearcherIds(researcherId.toString());
        algorithmRepository.save(algorithm);
    }
}
