package org.tfg.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tfg.application.DatasetDetailDto;
import org.tfg.application.DatasetDto;
import org.tfg.application.RankingDto;
import org.tfg.application.RankingRowDto;
import org.tfg.dataobjects.Algorithm;
import org.tfg.dataobjects.Dataset;
import org.tfg.dataobjects.Media;
import org.tfg.infrastructure.persistence.AlgorithmRepository;
import org.tfg.infrastructure.persistence.DatasetRepository;
import org.tfg.services.mediastorage.StorageManager;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DatasetsService {
    private final DatasetRepository datasetRepository;
    private final AlgorithmRepository algorithmRepository;
    private final StorageManager storageManager;

    private static final Map<Long, List<Double>> evaluations = new HashMap<Long, List<Double>>() {{
        put(1L, Arrays.asList(4.262, 2.040, 22.369, 4.083, 1.715, 1.287, 0.582, 2.343, 27.154));
        put(2L, Arrays.asList(4.528, 2.723, 19.248, 5.050, 2.573, 1.713, 0.872, 3.114, 26.063));
        put(3L, Arrays.asList(4.566, 2.216, 23.732, 4.664, 2.017, 1.222, 0.893, 2.902, 26.810));
    }};

    @Autowired
    public DatasetsService(DatasetRepository datasetRepository, AlgorithmRepository algorithmRepository, StorageManager storageManager) {
        this.datasetRepository = datasetRepository;
        this.algorithmRepository = algorithmRepository;
        this.storageManager = storageManager;
    }

    public List<DatasetDto> getDatasets(Long collectionId) {
        final Iterable<Dataset> all = datasetRepository.findAll();
        final List<Dataset> datasets = new ArrayList<>();
        all.forEach(d -> {
            if (d.isInCollection(collectionId)) datasets.add(d);
        });
        return datasets.stream()
                .map(d -> {
                    final Optional<Media> media = d.getMedia().stream().findFirst();
                    String thumbnail = "";
                    if (media.isPresent()) thumbnail = storageManager.getMediaResource(media.get()).base64Resource;
                    return new DatasetDto(d.getId().toString(), d.getName(), thumbnail);
                }).collect(Collectors.toList());
    }

    public DatasetDetailDto getDataset(Long datasetId) {
        final Dataset dataset = datasetRepository.findOne(datasetId);
        final Iterable<Algorithm> algorithms = algorithmRepository.findAll();
        final List<Algorithm> filtered = new ArrayList<>();
        algorithms.forEach(a -> {
            final List<Long> ids = Arrays.stream(a.getDatasetIds().split("")).map(Long::parseLong).collect(Collectors.toList());
            if (ids.contains(dataset.getId())) filtered.add(a);
        });
        List<String> headerColumnNames = Arrays.asList("EPE all", "EPE matched", "EPE unmatched", "d0-10", "d10-60", "d60-140", "s0-10", "s10-40", "s40+");
        final RankingDto ranking = new RankingDto(headerColumnNames, createRowDtosFrom(filtered));
        final List<String> thumbnails = dataset.getMedia().stream()
                .filter(m -> m.isForDataset(datasetId))
                .map(storageManager::getMediaResource)
                .map(r -> r.base64Resource)
                .collect(Collectors.toList());
        return new DatasetDetailDto(dataset.getId().toString(), dataset.getName(), thumbnails, dataset.hasVideo(), dataset.getVideoId(), ranking);
    }

    private List<RankingRowDto> createRowDtosFrom(List<Algorithm> algorithms) {
        return algorithms.stream()
                .map(a -> new RankingRowDto(a.getName(), evaluations.getOrDefault(a.getId(), new ArrayList<>())))
                .collect(Collectors.toList());
    }
}
