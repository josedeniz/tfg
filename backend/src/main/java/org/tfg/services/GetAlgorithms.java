package org.tfg.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tfg.application.AlgorithmDto;
import org.tfg.dataobjects.Algorithm;
import org.tfg.dataobjects.Dataset;
import org.tfg.infrastructure.persistence.AlgorithmRepository;
import org.tfg.infrastructure.persistence.DatasetRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GetAlgorithms {

    private final AlgorithmRepository algorithmRepository;
    private final DatasetRepository datasetRepository;

    @Autowired
    public GetAlgorithms(AlgorithmRepository algorithmRepository, DatasetRepository datasetRepository) {
        this.algorithmRepository = algorithmRepository;
        this.datasetRepository = datasetRepository;
    }

    public List<AlgorithmDto> execute(Long userId) {
        final Iterable<Algorithm> all = algorithmRepository.findAll();
        final ArrayList<AlgorithmDto> algorithms = new ArrayList<>();
        all.forEach(a -> {
            if (a.getResearcherIds().contains(String.valueOf(userId))) {
                final List<String> datasets = Arrays.stream(a.getDatasetIds().split(""))
                        .map(Long::parseLong)
                        .map(datasetRepository::findOne)
                        .map(Dataset::getName)
                        .collect(Collectors.toList());
                algorithms.add(new AlgorithmDto(a.getOriginalFilename(), datasets));
            }
        });
        return algorithms;
    }
}
