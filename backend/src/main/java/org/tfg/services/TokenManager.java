package org.tfg.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.tfg.dataobjects.Researcher;

import javax.servlet.http.HttpServletRequest;

@Service
public class TokenManager {

    private final GetResearcher getResearcher;

    @Autowired
    public TokenManager(GetResearcher getResearcher) {
        this.getResearcher = getResearcher;
    }

    public Researcher getUserFromToken(HttpServletRequest request) {
        UserDetails user = (UserDetails)request.getAttribute("user");
        return getResearcher.execute(user.getUsername());
    }
}
