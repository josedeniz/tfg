package org.tfg.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tfg.application.ResourceDto;
import org.tfg.application.ResourceIdDto;
import org.tfg.dataobjects.Media;
import org.tfg.infrastructure.persistence.MediaRepository;
import org.tfg.services.mediastorage.StorageManager;

import java.util.List;

@Service
public class GetMedia {

    private final StorageManager storageManager;
    private final MediaRepository mediaRepository;

    @Autowired
    public GetMedia(StorageManager storageManager, MediaRepository mediaRepository) {
        this.storageManager = storageManager;
        this.mediaRepository = mediaRepository;
    }

    public List<ResourceIdDto> getMediaResources() {
        final Iterable<Media> all = mediaRepository.findAll();
        return storageManager.getMediaResources(all);
    }

    public List<ResourceIdDto> getMediaResourcesByDataset(Long datasetId) {
        final Iterable<Media> all = mediaRepository.findAll();
        return storageManager.getMediaResourcesByDataset(datasetId, all);
    }

    public ResourceDto getMediaResource(String resourceId) {
        final Media media = mediaRepository.findByName(resourceId);
        return storageManager.getMediaResource(media);
    }
}
