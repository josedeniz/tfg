package org.tfg.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tfg.application.AboutDto;
import org.tfg.application.CollectionDetailDto;
import org.tfg.application.CollectionDto;
import org.tfg.dataobjects.Collection;
import org.tfg.infrastructure.persistence.CollectionRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CollectionsService {

    private final CollectionRepository collectionRepository;

    @Autowired
    public CollectionsService(CollectionRepository collectionRepository) {
        this.collectionRepository = collectionRepository;
    }

    public List<CollectionDto> getCollections() {
        final Iterable<Collection> all = collectionRepository.findAll();
        final List<CollectionDto> collections = new ArrayList<>();
        all.forEach(c -> collections.add(new CollectionDto(c.getId().toString(), c.getName())));
        return collections;
    }

    public CollectionDetailDto getCollection(Long id) {
        final Collection collection = collectionRepository.findOne(id);
        return new CollectionDetailDto(collection.getId().toString(), collection.getName(), collection.getAbout());
    }
}
