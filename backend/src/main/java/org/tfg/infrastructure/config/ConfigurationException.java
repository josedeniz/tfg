package org.tfg.infrastructure.config;

public class ConfigurationException extends Exception {
    public ConfigurationException(String message) {
        super(message);
    }
}
