package org.tfg.infrastructure.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.tfg.dataobjects.Researcher;
import org.tfg.infrastructure.persistence.ResearcherRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    private final ResearcherRepository researcherRepository;

    @Autowired
    public JwtUserDetailsService(ResearcherRepository researcherRepository) {
        this.researcherRepository = researcherRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Researcher researcher = researcherRepository.findByEmail(email);

        if (researcher == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", email));
        } else {
            return JwtUserFactory.create(researcher);
        }
    }
}
