package org.tfg.infrastructure.security;

import java.io.Serializable;

public class JwtRefreshTokenRequest implements Serializable {

    private String token;

    public String getToken() {
        return token;
    }

    public JwtRefreshTokenRequest setToken(String token) {
        this.token = token;
        return this;
    }
}
