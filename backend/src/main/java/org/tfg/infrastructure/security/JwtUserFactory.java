package org.tfg.infrastructure.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.tfg.dataobjects.Researcher;
import org.tfg.dataobjects.Role;

import java.util.List;
import java.util.stream.Collectors;

public class JwtUserFactory {
    private JwtUserFactory() {
    }

    public static JwtUser create(Researcher researcher) {
        return new JwtUser(
                researcher.getId(),
                researcher.getEmail(), // username
                researcher.getEmail(),
                researcher.getPassword(),
                mapToGrantedAuthorities(researcher.getRoles()),
                researcher.getEnabled()
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<Role> authorities) {
        return authorities.stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getValue()))
                .collect(Collectors.toList());
    }
}
