package org.tfg.infrastructure.security;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class JwtAuthenticationRequest implements Serializable {

    @JsonProperty("email")
    private String email;
    @JsonProperty("password")
    private String password;

    // only for deserialize
    public JwtAuthenticationRequest() {
    }

    public JwtAuthenticationRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public JwtAuthenticationRequest setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
