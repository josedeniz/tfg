package org.tfg.infrastructure.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.tfg.dataobjects.Researcher;
import org.tfg.dataobjects.Role;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class JwtAuthenticationResponse implements Serializable {

    @JsonProperty("name")
    private final String name;
    @JsonProperty("authorization_token")
    private final String authorizationToken;
    @JsonProperty("refresh_token")
    private String refreshToken;
    @JsonProperty("roles")
    private final List<Role> roles;

    public JwtAuthenticationResponse(String authorizationToken, String refreshToken) {
        this.name = "";
        this.roles = new ArrayList<>();
        this.authorizationToken = authorizationToken;
        this.refreshToken = refreshToken;
    }

    public JwtAuthenticationResponse(Researcher researcher, String authorizationToken, String refreshToken) {
        this.name = researcher.getName();
        this.roles = researcher.getRoles();
        this.authorizationToken = authorizationToken;
        this.refreshToken = refreshToken;
    }

    public String getName() {
        return name;
    }

    public String getAuthorizationToken() {
        return this.authorizationToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public List<Role> getRoles() {
        return roles;
    }
}
