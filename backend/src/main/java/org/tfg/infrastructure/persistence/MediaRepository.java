package org.tfg.infrastructure.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.tfg.dataobjects.Media;

@Repository
public interface MediaRepository extends CrudRepository<Media, Long> {
    Media findByName(String name);
}
