package org.tfg.infrastructure.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.tfg.dataobjects.Category;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {
}
