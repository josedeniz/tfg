package org.tfg.infrastructure.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.tfg.dataobjects.Researcher;

@RepositoryRestResource(collectionResourceRel = "researchers", path = "researchers")
public interface ResearcherRepository extends CrudRepository<Researcher, Long> {
    Researcher findByEmail(String email);
}
