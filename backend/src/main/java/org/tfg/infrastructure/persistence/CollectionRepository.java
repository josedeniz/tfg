package org.tfg.infrastructure.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.tfg.dataobjects.Collection;

@Repository
public interface CollectionRepository extends CrudRepository<Collection, Long> {
}
