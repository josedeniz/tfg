package org.tfg.infrastructure.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.tfg.dataobjects.Dataset;

@Repository
public interface DatasetRepository extends CrudRepository<Dataset, Long> {
}
