package org.tfg.infrastructure.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.tfg.dataobjects.Algorithm;

@Repository
public interface AlgorithmRepository extends CrudRepository<Algorithm, Long> {
    Algorithm findByFilename(String filename);
}
