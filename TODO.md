### TODO List

- [X] BitBucket pipelines
- [X] CRUD Users
- [X] Encrypt password
- [X] Migrations?
- [X] Set by config if the Storage is with AWS or  local folder
- [X] Configuration Exceptions
- [X] Separate into maven modules
- [X] Email unique
- [X] Login OAuth (front)
- [X] Resolve images path in front
- [X] Bug: multiple files are replaced because of the timestamp
- [ ] DB non volatile. Fix migrations
- [ ] Entity relations (Collection, Databases, Datasets, Media)
- [ ] Results table (CRUD)

### Technical Debts
- [X] Change language
- [ ] Register: validate 2 passwords are equal
- [ ] Login / Register error messages
- [ ] Reset password

### Extra
- [ ] Exceptionless or similar
- [ ] Remove hash Vue router (conflict with backend routing)

