import colors from 'vuetify/es5/util/colors';

export default {
    applicationName: 'Research App',
    defaultLanguage: 'en',
    languages: ['en', 'es'],
    theme: {
        primary: '#5c88aa',
        'primary-light': '#8cb8dc',
        'primary-dark': '#2c5b7b',
        'gray-background': '#e1e2e1',
        secondary: colors.teal.darken4,
        accent: '#0097a7',
        error: colors.red.darken2,
        warning: colors.yellow.darken2,
        info: colors.lightBlue.darken2,
        success: colors.green.base
    }
};
