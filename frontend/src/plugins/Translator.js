import en from './en';
import es from './es';
import Vue from 'vue';
import vuexI18n from 'vuex-i18n';

export default class Translator {
    static initializePlugin(store) {
        Vue.use(vuexI18n.plugin, store);
        Vue.i18n.add('en', en);
        Vue.i18n.add('es', es);
        Vue.i18n.set(store.state.selectedLanguage);
    }
}
