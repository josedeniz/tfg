import config from '../../config/config';

export default {
    currencyFilter: () => {
        return aFilter('formatCurrency',
            (value, selectedLanguage) => selectedLanguage === config.defaultLanguage ? `£${value}` : `${value}€`);
    },
    toFixedFilter: () => {
        return aFilter('toFixed', (price, limit) => price.toFixed(limit));
    }
};

function aFilter(name, action) {
    return {
        name,
        action
    };
}
