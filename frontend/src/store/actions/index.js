import * as actionTypes from '../actionTypes';
import { researchersClient, collectionsClient, algorithmsClient } from '../../infrastructure/restClientsFactory';

export default {

    [actionTypes.LOGIN_RESEARCHER]: (context, credentials) => {
        return researchersClient.loginResearcher(credentials);
    },
    [actionTypes.GET_RESEARCHER]: (context, researcherId) => {
        return researchersClient.getResearcher(researcherId);
    },
    [actionTypes.REGISTER_RESEARCHER]: (context, credentials) => {
        return researchersClient.registerResearcher(credentials);
    },
    [actionTypes.GET_COLLECTION]: (context, collectionId) => {
        return collectionsClient.getCollection(collectionId);
    },
    [actionTypes.GET_COLLECTIONS]: async (context) => {
        const collections = await collectionsClient.getCollections();
        context.commit(actionTypes.GET_COLLECTIONS, collections);
    },
    [actionTypes.GET_DATASETS]: (context, collectionId) => {
        return collectionsClient.getDatasets(collectionId);
    },
    [actionTypes.GET_DATASET]: (context, { collectionId, datasetId }) => {
        return collectionsClient.getDataset(collectionId, datasetId);
    },
    [actionTypes.GET_ALGORITHMS]: () => {
        return algorithmsClient.getAlgorithms();
    }
};
