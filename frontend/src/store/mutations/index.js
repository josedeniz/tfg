import * as actionTypes from '../actionTypes';

export default {
    [actionTypes.CHANGE_LANGUAGE]: (state, selectedLanguage) => {
        state.selectedLanguage = selectedLanguage;
    },
    [actionTypes.GET_COLLECTIONS]: (state, collections) => {
        state.collections = collections;
    }
};
