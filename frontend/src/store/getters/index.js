export default {
    selectedLanguage(state) {
        return state.selectedLanguage;
    },
    getCollections(state) {
        return state.collections.map(c => ({...c}));
    }
};
