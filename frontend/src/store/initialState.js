import config from '../../config/config';

function browserLanguage() {
    const locale = (window.navigator.userLanguage || window.navigator.language);
    if (locale.length >= 5) {
        const lang = locale.substring(0, 2).toLowerCase();
        return config.languages.includes(lang) ? lang : config.defaultLanguage;
    }
    return locale;
}

export default {
    selectedLanguage: browserLanguage(),
    collections: []
};
