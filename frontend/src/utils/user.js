import storageWrapper from './localStorageWrapper';
import { DATABASE_MANAGER_ROLE } from './roles';

const userKey = 'user';

function create(userData) {
    storageWrapper.set(userKey, userData);
}

function getData() {
    return storageWrapper.get(userKey);
}

function getAuthorizationToken() {
    const sessionUser = getData();
    return sessionUser ? sessionUser.authorization_token : '';
}

function clear() {
    storageWrapper.delete(userKey);
}

function exists() {
    return storageWrapper.has(userKey);
}

function update(response) {
    const user = storageWrapper.get(userKey);
    const newTokens = {
        authorization_token: response.authorization_token,
        refresh_token: response.refresh_token
    };
    storageWrapper.set(userKey, Object.assign(user, newTokens));
}

function isDatabaseManager() {
    return hasRole(DATABASE_MANAGER_ROLE);
}

function hasRole(role) {
    const user = getData();
    return exists() ? user.roles.includes(role) : false;
}

export default {
    create,
    getData,
    clear,
    exists,
    update,
    isDatabaseManager,
    getAuthorizationToken
};
