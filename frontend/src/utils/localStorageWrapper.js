export default {
    set: (key, item) => {
        localStorage.setItem(key, JSON.stringify(item));
    },
    has: (key) => {
        return localStorage.getItem(key) !== null;
    },
    get: (key) => {
        return JSON.parse(localStorage.getItem(key));
    },
    reset: () => {
        localStorage.clear();
    },
    delete: (key) => {
        localStorage.removeItem(key);
    }
};
