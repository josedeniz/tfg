import Vue from 'vue';
import App from './App';
import 'babel-polyfill';
import './assets/style.css';
import LRU from 'lru-cache';
import Vuetify from 'vuetify';
import router from './router';
import store from './store/index';
import config from '../config/config';
import filters from './filters/filters';
import Translator from './plugins/Translator';
import './styles/style.css';
import '../node_modules/vuetify/dist/vuetify.min.css';
import '../node_modules/mdi/css/materialdesignicons.min.css';
import VueYouTubeEmbed from 'vue-youtube-embed';

Vue.config.productionTip = false;

const themeCache = LRU({
    max: 10,
    maxAge: 1000 * 60 * 60 // 1 hour
});
Vue.use(Vuetify, {
    theme: config.theme,
    minifyTheme: () => {
        /* eslint-disable no-undef */
        return process.env.NODE_ENV === 'production'
            ? val.replace(/[\s|\r\n|\r|\n]/g, '')
            : null;
        /* eslint-enable */
    },
    options: {
        themeCache
    }
});
Translator.initializePlugin(store);

Vue.use(VueYouTubeEmbed, { global: true, componentId: 'youtube-media' });

const currencyFilter = filters.currencyFilter();
Vue.filter(currencyFilter.name, currencyFilter.action);
const toFixedFilter = filters.toFixedFilter();
Vue.filter(toFixedFilter.name, toFixedFilter.action);

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
});
