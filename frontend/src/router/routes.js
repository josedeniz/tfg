export const HOME_ROUTE = '/';
export const LOGIN_ROUTE = '/login';
export const REGISTER_ROUTE = '/register';
export const COLLECTION_DETAIL_ROUTE = 'collections';
export const DATASET_ROUTE = 'datasets';
export const ALGORITHMS_ROUTE = '/algorithms';
