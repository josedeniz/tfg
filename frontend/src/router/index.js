import Vue from 'vue';
import user from '../utils/user';
import Router from 'vue-router';
import Home from '../components/Home';
import Algorithms from '../components/Algorithms';
import Login from '../components/Login';
import Register from '../components/Register';
import PageContainer from '../components/PageContainer';
import * as routes from './routes';
import CollectionDetail from '../components/CollectionDetail';
import DatasetDetail from '../components/DatasetDetail';

Vue.use(Router);

/* eslint-disable */
function hasToken(to, from, next) {
    const sessionUser = user.getData();
    if (sessionUser && sessionUser.authorization_token) {
        next();
    } else {
        next(routes.LOGIN_ROUTE);
    }
}
/* eslint-enable */

export default new Router({
    // mode: 'history',
    routes: [
        {
            path: routes.LOGIN_ROUTE,
            name: routes.LOGIN_ROUTE,
            title: 'Login',
            component: Login
        },
        {
            path: routes.REGISTER_ROUTE,
            name: routes.REGISTER_ROUTE,
            title: 'Register',
            component: Register
        },
        {
            path: '',
            component: PageContainer,
            children: [
                {
                    path: routes.HOME_ROUTE,
                    name: routes.HOME_ROUTE,
                    title: 'Home',
                    component: Home
                },
                {
                    path: routes.ALGORITHMS_ROUTE,
                    name: routes.ALGORITHMS_ROUTE,
                    title: 'Algorithms',
                    component: Algorithms
                },
                {
                    path: `${routes.COLLECTION_DETAIL_ROUTE}/:id`,
                    name: routes.COLLECTION_DETAIL_ROUTE,
                    title: 'Collection detail',
                    component: CollectionDetail
                },
                {
                    path: `${routes.COLLECTION_DETAIL_ROUTE}/:id/${routes.DATASET_ROUTE}/:datasetId`,
                    name: routes.DATASET_ROUTE,
                    title: 'Dataset detail',
                    component: DatasetDetail
                }
            ]
        },
        {
            path: '*',
            redirect: routes.HOME_ROUTE
        }
    ]
});
