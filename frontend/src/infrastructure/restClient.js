import 'whatwg-fetch';
import user from '../../src/utils/user';
import * as apiRoutes from './apiRoutes';

export default function restClient() {
    const defaultCredentials = () => {
        const loggedInUser = user.getData();
        return {
            Authorization: loggedInUser ? `Bearer ${loggedInUser.authorization_token}` : ''
        };
    };

    const getUserRefreshToken = () => {
        const userData = user.getData();
        return userData.refresh_token;
    };

    const defaultHeaders = () => {
        return Object.assign({'Content-type': 'Application/json'}, defaultCredentials());
    };

    function get(url) {
        return doRequest('GET', url);
    }

    function post(url, data) {
        return doRequest('POST', url, data);
    }

    function put(url, data) {
        return doRequest('PUT', url, data);
    }

    function parseStatusCode(response) {
        const successStatusCodes = [200, 201, 203, 204];
        if (successStatusCodes.includes(response.status)) {
            return Promise.resolve(response);
        } else {
            return response.json()
                .catch(() => {
                    const error = generateServerError(response);
                    return Promise.reject(error);
                })
                .then(json => {
                    return Promise.reject(json);
                });
        }
    }

    function generateServerError(response) {
        return new Error(response.statusText);
    }

    function parseJsonResponse(response) {
        try {
            if (response.status === 204) return Promise.resolve({});
            return Promise.resolve(response.json());
        } catch (exception) {
            const message = 'Client is expecting JSON but server is returning HTML. Are you sure you are running the right backend?';
            /* eslint-disable */
            console.error(message);
            return Promise.reject({message});
            /* eslint-enable */
        }
    }

    function doRequest(method, url, data = undefined, headers = defaultHeaders()) {
        return fetch(url, {
            async: true,
            method: method,
            headers: headers,
            body: JSON.stringify(data)
        })
            .catch((error) => {
                /* eslint-disable */
                return Promise.reject({message: `Network problem: ${error.toString()}`});
                /* eslint-enable */
            })
            .then(parseStatusCode)
            .then(parseJsonResponse);
    }

    function uploadFile(url, data = undefined, headers = defaultCredentials()) {
        return fetch(url, {
            async: true,
            method: 'POST',
            headers: headers,
            body: data
        })
            .catch((error) => {
                console.log('error:', error);
            })
            .then(parseStatusCode)
            .then(parseJsonResponse);
    }

    function handleUnauthorizedResponse(response, user) {
        if (response.status === 401) {
            user.clear();
        }
    }
    /* eslint-disable no-unused-vars */

    function refreshToken() {
        return post(apiRoutes.REFRESH_TOKEN_ROUTE, getUserRefreshToken())
            .then((response) => {
                user.update(response);
            }, (error) => {
                handleUnauthorizedResponse(error, user);
                return Promise.reject(error);
            });
    }

    function isNotAuthorized(error) {
        return error.code === 401;
    }

    function isForbidden(error) {
        return error.code === 403;
    }
    /* eslint-enable */

    return {
        get,
        post,
        put,
        uploadFile
    };
}
