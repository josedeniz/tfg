/* eslint-disable */
import * as apiRoutes from './apiRoutes';
import { DATABASE_MANAGER_ROLE, USER_ROLE } from '../utils/roles';

const fakeResponses = {
    [apiRoutes.LOGIN_ROUTE]: () => {
        return Promise.resolve({
            authorization_token: "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbnlAZW1haWwuY29tIiwiZXhwIjoxNTI3NDU0MDI5LCJpYXQiOjE1Mjc0NTM5Njl9.6QLGCId6iOpMJVkg2zdhgGvHJpqP3_0ze9emZpM2n4sfL3aSshJxJkTC5XuiP5d-xGnYEc5hPyX1kHfD9B_hVA",
            refresh_token: "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbnlAZW1haWwuY29tIiwiZXhwIjoxNTI3NDU0MDg5LCJpYXQiOjE1Mjc0NTM5Njl9.3paXHjl5MxHCfSYUIHdfMlH_V_0vFkO56pLMqNiF22BI0mE1JB3PEJziuO73bHW_IwcwNmo9cL_WX9FYgONnJg",
            roles: [DATABASE_MANAGER_ROLE, USER_ROLE],
            name: 'Jose'
        });
    },
    [apiRoutes.REGISTER_ROUTE]: (data) => {
        return Promise.resolve({
            email: data.email
        });
    }
};

export default function FakeRestClient() {

    function get() {
        return Promise.resolve();
    }

    function post(route, data) {
        const fakeResponse = fakeResponses[route];
        return !fakeResponse ? Promise.resolve() : fakeResponse(data);
    }

    function put() {
        return Promise.resolve();
    }

    function uploadFile() {
        return Promise.resolve();
    }

    return {
        get,
        post,
        put,
        uploadFile
    }
};
