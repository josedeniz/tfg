import user from '../../utils/user';
import * as apiRoutes from '../apiRoutes';

export default function ResearchersClient(restClient) {
    function registerResearcher(credentials) {
        return restClient.post(apiRoutes.REGISTER_ROUTE, credentials);
    }

    function loginResearcher(credentials) {
        return restClient.post(apiRoutes.LOGIN_ROUTE, credentials)
            .then(response => user.create(response));
    }

    function getResearcher(userId) {
        return restClient.get(`${apiRoutes.RESEARCHERS_ROUTE}/${userId}`);
    }

    return {
        getResearcher,
        loginResearcher,
        registerResearcher
    };
}
