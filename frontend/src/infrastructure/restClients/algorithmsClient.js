import * as apiRoutes from '../apiRoutes';

export default function AlgorithmsClient(restClient) {
    function getAlgorithms() {
        return restClient.get(apiRoutes.ALGORITHMS_ROUTE);
    }

    return {
        getAlgorithms
    };
}
