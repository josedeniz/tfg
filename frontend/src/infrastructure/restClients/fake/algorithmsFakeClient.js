import AlgorithmsClient from '../algorithmsClient';

export default function AlgorithmsFakeClient() {
    const algorithms = [
        {
            filename: 'teddy.c',
            datasets: 'Teddy'
        },
        {
            filename: 'adventurer.c',
            datasets: 'Adventurer'
        }
    ];

    function getAlgorithms() {
        return Promise.resolve(algorithms);
    }

    return {
        ...AlgorithmsClient,
        getAlgorithms
    };
}
