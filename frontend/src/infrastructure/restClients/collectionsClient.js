import * as apiRoutes from '../apiRoutes';

export default function CollectionsClient(restClient) {
    function getCollection(collectionId) {
        return restClient.get(`${apiRoutes.COLLECTIONS_ROUTE}/${collectionId}`);
    }

    function getCollections() {
        return restClient.get(apiRoutes.COLLECTIONS_ROUTE);
    }

    function getDatasets(collectionId) {
        return restClient.get(apiRoutes.DATASETS_ROUTE(collectionId));
    }

    function getDataset(collectionId, datasetId) {
        return restClient.get(apiRoutes.DATASET_ROUTE(collectionId, datasetId));
    }

    return {
        getCollection,
        getCollections,
        getDatasets,
        getDataset
    };
}
