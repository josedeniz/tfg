import RestClient from './restClient';
import FakeRestClient from './fakeRestClient';
import ResearchersClient from './restClients/researchersClient';
import CollectionsClient from './restClients/collectionsClient';
import CollectionsFakeClient from './restClients/fake/collectionsFakeClient';
import AlgorithmsClient from './restClients/algorithmsClient';
import AlgorithmsFakeClient from './restClients/fake/algorithmsFakeClient';

const useFakeRestClient = __FAKE_REST_CLIENT__; /* eslint-disable-line */
const restclient = useFakeRestClient ? FakeRestClient() : RestClient();
export const researchersClient = ResearchersClient(restclient);
export const collectionsClient = useFakeRestClient ? CollectionsFakeClient() : CollectionsClient(restclient);
export const algorithmsClient = useFakeRestClient ? AlgorithmsFakeClient() : AlgorithmsClient(restclient);
